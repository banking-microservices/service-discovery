# Bank Management Service Discovery

### Description

This is a Spring Cloud Service Discovery Application where the other microservices can register them for inter service communication.

This spring boot project written in **Java 11**.

# Installation (How to run in local??)

> Clone the project
>
> Build the application with `mvn clean install`
>
> Run the Application with `mvn spring-boot:run`
>
> visit http://localhost:5000/
--------------------------------------------------

# Environment Variable(s)
SERVICE_DISCOVERY_PORT - To change the server port
 
